# A collection of ramda recipes

Filter object by array of keys [REPL](http://ramdajs.com/repl/#?code=var%20filterByKeys%20%3D%20R.curry%28function%20filterByKeys%28keys%2C%20object%29%20%7B%0A%20%20%20%20var%20contains%20%3D%20R.contains%28R.__%2C%20keys%29%3B%0A%20%20%20%20return%20R.compose%28%0A%20%20%20%20%20%20%20%20R.fromPairs%2C%0A%20%20%20%20%20%20%20%20R.filter%28function%28pair%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20contains%28pair%5B0%5D%29%3B%0A%20%20%20%20%20%20%20%20%7D%29%2C%0A%20%20%20%20%20%20%20%20R.toPairs%0A%20%20%20%20%29%28object%29%3B%0A%7D%29%3B%0A%0AfilterByKeys%28%5B%27b%27%2C%20%27c%27%2C%20%27d%27%5D%2C%20%7Ba%3A%20%27x%27%2C%20b%3A%20%27y%27%2C%20c%3A%20%27z%27%7D%29%3B)
```javascript
var filterByKeys = R.curry(function filterByKeys(keys, object) {
    var contains = R.contains(R.__, keys);
    return R.compose(
        R.fromPairs,
        R.filter(function(pair) {
            return contains(pair[0]);
        }),
        R.toPairs
    )(object);
});
```

Filter array of objects by a property in a list
    [REPL](http://ramdajs.com/repl/#?code=var%20servers%20%3D%20%5B%7Bid%3A%201%7D%2C%20%7Bid%3A%202%7D%2C%20%7Bid%3A%203%7D%5D%3B%0Avar%20knownIds%20%3D%20%5B1%2C%202%5D%3B%0Avar%20findByPropList%20%3D%20R.curry%28function%20findByPropInList%28prop%2C%20list%2C%20item%29%20%7B%0A%20%20%20return%20R.contains%28item%5Bprop%5D%2C%20list%29%3B%0A%7D%29%3B%0Avar%20findByIdInKnownIds%20%3D%20findByPropList%28%27id%27%2C%20knownIds%29%3B%0AR.filter%28findByIdInKnownIds%2C%20servers%29%3B%0A)
```javascript
var knownIds = [1, 2];
var findByPropList = R.curry(function findByPropInList(prop, list, item) {
   return R.contains(item[prop], list);
});
var findByIdInKnownIds = findByPropList('id', knownIds);
```
